![npm (custom registry)](https://img.shields.io/npm/v/@thearc/eslint-config/latest?color=green&registry_uri=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fpackages%2Fnpm%2F&style=for-the-badge)

# ESLint Configuration for THE ARC

Additional [Rules](https://eslint.org/docs/rules/) can be found on the eslint website.

Contains the ESLint configuration used for projects maintained by the THE ARC team.

## Installation

You can install ESLint using npm:

    npm install eslint --save-dev
or using yarn

    yarn add -D eslint

Add the gitlab registry to your project by adding either a `.npmrc` file (when using npm) or `.yarnrc` (when using yarn)

*`.npmrc`*

    "@thearc:registry"="https://gitlab.com/api/v4/projects/32946238/packages/npm/"

*`.yarnrc`*

    "@thearc:registry" "https://gitlab.com/api/v4/projects/32946238/packages/npm/"

Then install this configuration:

    npm install @thearc/eslint-config --save-dev
or

    yarn add -D @thearc/eslint-config


## Update

Just change the rules i either the `default.yml` or the `index.js`.

Change the version to a new release number.

Commit and push. GitLab CI will do the rest.

## Usage

In your `package.json` file, add:

```json
{
    "extends": "@thearc/eslint-config"
}
```
